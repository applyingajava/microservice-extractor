package com.aj.tools.extractor.adapters;

import com.aj.tools.extractor.beans.ClassFile;
import com.aj.tools.extractor.beans.FieldInvokerBean;
import com.aj.tools.extractor.beans.MethodInvokerBean;
import com.aj.tools.extractor.constant.ServiceExtractorConstants;
import com.aj.tools.extractor.helper.EventRecordHelper;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;
import org.objectweb.asm.commons.RemappingMethodAdapter;

import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.regex.Matcher;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class MethodRemappingClassAdapter extends RemappingClassAdapter {
    private static final Logger LOGGER = Logger.getLogger(MethodRemappingClassAdapter.class.getName());

    public MethodRemappingClassAdapter(ClassVisitor cv, Remapper remapper) {
        super(cv, remapper);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc,
                                     String signature, String[] exceptions) {
        LOGGER.fine("MethodVisitor.visitMethod( " + name + " , " + desc + ")");
        String newDesc = remapper.mapMethodDesc(desc);
        MethodVisitor mv = super.visitMethod(access,
                remapper.mapMethodName(className, name, desc),
                newDesc,
                remapper.mapSignature(signature, false),
                exceptions == null ? null : remapper.mapTypes(exceptions));
        MethodMappingMethodAdapter methodAdapter = new MethodMappingMethodAdapter(mv, name, desc);
        return new RemappingMethodAdapter(access, desc, methodAdapter, remapper);
    }

    /**
     * MethodMappingMethodAdapter
     */
    private class MethodMappingMethodAdapter extends MethodVisitor {
        private Set<MethodInvokerBean> methodCallers;
        private Set<FieldInvokerBean> fieldCallers;

        private String actualClassName;
        private String actualMethodName;
        private String actualMethodDesc;

        public MethodMappingMethodAdapter(MethodVisitor mv, String name, String desc) {
            super(Opcodes.ASM5, mv);
            //checking the pattern to see if its a lambda function or not
            Matcher matcher = ServiceExtractorConstants.LAMBDA_METHOD_PATTERN.matcher(name);
            if (matcher.find()) {
                // as all lambdas function of any method would start with "lambda" word
                // followed by parent method name and occurrence number so pulling second word from the lambda function
                // for its parent method name
                //
                //ex: lambda$StringBuilder$9
                try {
                    String methodName = name.split("\\$")[1];
                    ClassFile classFile = EventRecordHelper.getCurrentClass();
                    Optional<ClassFile.Method> methodOptional = classFile.getActiveMethods()
                            .stream().filter(method -> method.getMethodName().contains(methodName))
                            .findFirst();
                    if (methodOptional.isPresent()) {
                        ClassFile.Method method = methodOptional.get();
                        this.methodCallers = method.getMethodCallers();
                        this.fieldCallers = method.getFieldCallers();
                        this.actualMethodName = method.getMethodName();
                        this.actualMethodDesc = method.getDesc();
                        return;
                    }
                } catch (Exception ex) {
                    LOGGER.severe("Exception while parsing the lambda expression, " +
                            "so continuing with the normal construction creation." + ex.getStackTrace());
                }
            }
            this.methodCallers = new TreeSet<>();
            this.fieldCallers = new TreeSet<>();
            this.actualMethodName = name;
            this.actualMethodDesc = desc;
        }

        @Override
        public void visitCode() {
            LOGGER.fine("MethodMappingMethodAdapter.visitCode() for " + actualMethodName);
            EventRecordHelper.setActiveMethod(true);
            super.visitCode();
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String desc) {
            LOGGER.fine("MethodMappingMethodAdapter.visitMethodInsn( " + owner + " , " + name + " , " + desc + ")");
            MethodInvokerBean caller = new MethodInvokerBean(owner, name, desc);
            this.methodCallers.add(caller);
            super.visitMethodInsn(opcode, owner, name, desc);
        }

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
            LOGGER.fine("MethodMappingMethodAdapter.visitMethodInsn( " + owner + " , " + name + " , " + desc + ")");
            MethodInvokerBean caller = new MethodInvokerBean(owner, name, desc);
            this.methodCallers.add(caller);
            super.visitMethodInsn(opcode, owner, name, desc, itf);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String desc) {
            LOGGER.fine("MethodMappingMethodAdapter.visitFieldInsn( " + owner + " , " + name + " , " + desc + ")");
            FieldInvokerBean caller = new FieldInvokerBean(owner, name, desc);
            this.fieldCallers.add(caller);
            super.visitFieldInsn(opcode, owner, name, desc);
        }

        @Override
        public void visitEnd() {
            LOGGER.fine("MethodMappingMethodAdapter.visitEnd()" + actualMethodName);
            EventRecordHelper.getCurrentClass().addMethod(actualMethodName, actualMethodDesc, methodCallers, fieldCallers);
            EventRecordHelper.setActiveMethod(false);
            super.visitEnd();
        }


        public void setActualClassName(String actualClassName) {
            this.actualClassName = actualClassName;
        }

        public void setActualMethodName(String actualMethodName) {
            this.actualMethodName = actualMethodName;
        }

        public void setActualMethodDesc(String actualMethodDesc) {
            this.actualMethodDesc = actualMethodDesc;
        }

        public Set<MethodInvokerBean> getMethodCallers() {
            return methodCallers;
        }

        public Set<FieldInvokerBean> getFieldCallers() {
            return fieldCallers;
        }
    }

}
