package com.aj.tools.extractor.adapters;

import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.helper.EventRecordHelper;
import com.aj.tools.extractor.helper.ReflectionsHelper;
import com.aj.tools.extractor.factory.BeanFactory;
import org.apache.commons.lang3.StringUtils;
import org.objectweb.asm.commons.Remapper;

import java.util.Set;
import java.util.logging.Logger;

import static com.aj.tools.extractor.helper.ClassVisitorCache.getParsedClassNames;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class ClassInsRemapper extends Remapper {
    private static final Logger LOGGER = Logger.getLogger(ClassInsRemapper.class.getName());

    private final Set<Class<?>> classNames;
    private final String className;
    private final String prefix;

    public ClassInsRemapper(final Set<Class<?>> classNames, final String prefix,
                            String className) {
        this.classNames = classNames;
        this.className = className;
        this.prefix = prefix;
        LOGGER.info("visiting class : " + className);
        className = StringUtils.replace(className, "/", ".");
        EventRecordHelper.setCurrentClassName(className);
    }

    @Override
    public String mapFieldName(String owner, String name, String desc) {
        owner = StringUtils.replace(owner, "/", ".");
        if (!EventRecordHelper.isActiveMethod()) {
            LOGGER.fine("visiting field of : " + owner + " : " + name + " : " + desc);
        }
        EventRecordHelper.getCurrentClass(owner).addField(name, desc);
        owner = StringUtils.replace(owner, "/", ".");
        return super.mapFieldName(owner, name, desc);
    }

    @Override
    public String mapMethodName(String owner, String name, String desc) {
        LOGGER.fine("visiting method of : " + owner + " : " + name + " : " + desc);
        owner = StringUtils.replace(owner, "/", ".");
        return super.mapMethodName(owner, name, desc);
    }

    @Override
    public String mapDesc(final String desc) {
        if (desc.startsWith("L")) {
            this.addType(desc.substring(1, desc.length() - 1));
        }
        return super.mapDesc(desc);
    }

    @Override
    public String[] mapTypes(final String[] types) {
        for (final String type : types) {
            this.addType(type);
        }
        return super.mapTypes(types);
    }

    private void addType(final String type) {
        final String className = type.replace('/', '.');

        if (this.prefix != null && className.startsWith(this.prefix)) {
            if (className.startsWith(this.prefix)) {
                try {
                    this.classNames.add(Class.forName(className));
                } catch (final ClassNotFoundException e) {
                    e.toString();
                }
            }

        } else {
            try {
                if (!className.contains("jxbrowser")) {
                    if (className.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
                        if (getParsedClassNames().contains(className))
                            return;
                        Class<?> myObjectClass = BeanFactory.getFileClassLoader()
                                .loadClass(className);
                        if (myObjectClass.isInterface()) {
                            Set<String> subClasses = ReflectionsHelper.getSubClasses(myObjectClass);
                            if (getParsedClassNames().contains(className))
                                return;
                            subClasses.forEach(czName -> {
                                try {
                                    if (!getParsedClassNames().contains(czName)) {
                                        this.classNames.add(BeanFactory.getFileClassLoader()
                                                .loadClass(czName));
                                        getParsedClassNames().add(czName);
                                    }
                                } catch (Exception e) {
                                    LOGGER.severe("Error occured while processing class : " + className + " details: " + e);
                                }
                            });

                        }
                        this.classNames.add(myObjectClass);
                        getParsedClassNames().add(className);
                    } else {
                        this.classNames.add(Class.forName(className));
                    }
                }

            } catch (Exception e) {
                LOGGER.severe("Error occured while processing class : " + className + " details: " + e);
            }
        }
    }

    @Override
    public String mapType(final String type) {
        this.addType(type);
        return type;
    }
}
