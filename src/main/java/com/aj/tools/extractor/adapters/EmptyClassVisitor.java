package com.aj.tools.extractor.adapters;

import org.objectweb.asm.ClassVisitor;

/**
 * Created by harsha.vemireddy on 2/27/2017.
 */
public class EmptyClassVisitor extends ClassVisitor {
    public EmptyClassVisitor(int api) {
        super(api);
    }
}
