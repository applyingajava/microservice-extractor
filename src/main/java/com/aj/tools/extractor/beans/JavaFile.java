package com.aj.tools.extractor.beans;

import java.io.File;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class JavaFile extends File {
    private String fullClassName;
    private String dirName;
    private String fullFilePath;
    private String packageDir;

    public JavaFile(String fullFilePath) {
        super(fullFilePath);
        this.fullFilePath = fullFilePath;
    }

    public String getFullClassName() {
        return fullClassName;
    }

    public void setFullClassName(String fullClassName) {
        this.fullClassName = fullClassName;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public String getFullFilePath() {
        return fullFilePath;
    }

    public void setFullFilePath(String fullFilePath) {
        this.fullFilePath = fullFilePath;
    }

    public String getPackageDir() {
        return packageDir;
    }

    public void setPackageDir(String packageDir) {
        this.packageDir = packageDir;
    }

    @Override
    public boolean equals(Object obj) {
        JavaFile file = (JavaFile) obj;
        return (this.fullClassName.equals(file.getFullClassName()));
    }

    @Override
    public String toString() {
        return "JavaFile [fullClassName=" + fullClassName + ", dirName="
                + dirName + ", fullFilePath=" + fullFilePath + ", packageDir="
                + packageDir + "]";
    }

}