package com.aj.tools.extractor.beans;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by harsha.vemireddy on 2/23/2017.
 */
public class FieldInvokerBean implements Comparable<FieldInvokerBean> {

    private String className;
    private String fieldName;
    private String fieldDesc;

    public FieldInvokerBean(String className, String fieldName, String fieldDesc) {
        this.className = ClassFile.trimClassName(StringUtils.replace(className, "/", "."));
        this.fieldName = fieldName;
        this.fieldDesc = fieldDesc;
    }

    public String getClassName() {
        return className;
    }

    public String getFieldDesc() {
        return fieldDesc;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getSimpleClassName() {
        String simpleName = this.className;
        int index = this.className.lastIndexOf("/");
        if (index >= 0) {
            simpleName = this.className.substring(index + 1);
        }
        return simpleName;
    }

    public String getPackageName() {
        String packageName = "";
        int index = this.className.lastIndexOf("/");
        if (index >= 0) {
            packageName = this.className.substring(0, index).replace("/", ".");
        }
        return packageName;
    }

    @Override
    public int hashCode() {
        return (className + fieldName + fieldDesc).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == FieldInvokerBean.class && obj.hashCode() == this.hashCode();
    }

    public int compareTo(FieldInvokerBean o) {
        int comp = this.className.compareTo(o.className);
        if (comp == 0) {
            comp = this.fieldName.compareTo(o.fieldName);
            if (comp == 0) {
                comp = this.fieldDesc.compareTo(o.fieldDesc);
            }
        }
        return comp;
    }

    @Override
    public String toString() {
        return this.className + ((this.fieldName.equals("")) ? "" : "." + this.fieldName + this.fieldDesc);
    }
}