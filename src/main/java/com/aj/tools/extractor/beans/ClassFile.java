package com.aj.tools.extractor.beans;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class ClassFile {
    private String className;
    private String fullClassName;
    private boolean isInterface;
    private boolean isInherited;

    private Map<String, ClassFile> dependentClasses;
    private Set<Field> activeFields;
    private Set<Method> activeMethods;

    public ClassFile(String className, String fullClassName) {
        this.className = className;
        this.fullClassName = fullClassName;
        inti();
    }

    public ClassFile(String className) {
        className = trimClassName(className);
        this.className = className;
        inti();
    }

    private void inti() {
        activeFields = Collections.newSetFromMap(new ConcurrentHashMap<>());
        activeMethods = Collections.newSetFromMap(new ConcurrentHashMap<>());
        dependentClasses = new ConcurrentHashMap<>();
    }

    public Map<String, ClassFile> getDependentClasses() {
        return dependentClasses;
    }

    public Set<Field> getActiveFields() {
        return activeFields;
    }

    public Set<Method> getActiveMethods() {
        return activeMethods;
    }

    public boolean addMethod(String methodName, String desc, Set<MethodInvokerBean> methodCallers) {
        methodName = trimClassName(methodName);
        return getActiveMethods().add(new Method(methodName, desc, methodCallers));
    }

    public boolean addMethod(String methodName, String desc, Set<MethodInvokerBean> methodCallers, Set<FieldInvokerBean> fieldCallers) {
        methodName = trimClassName(methodName);
        return getActiveMethods().add(new Method(methodName, desc, methodCallers, fieldCallers));
    }

    public static String trimClassName(String className) {
        if (className.startsWith("[L")) {
            className = className.substring(2, className.length() - 1);
        }
        return className;
    }

    public boolean addField(String fieldName, String desc) {
        fieldName = trimClassName(fieldName);
        return getActiveFields().add(new Field(fieldName, desc));
    }

    public String getClassName() {
        return className;
    }

    public boolean isInherited() {
        return isInherited;
    }

    public void setInherited(boolean inherited) {
        isInherited = inherited;
    }

    public boolean isInterface() {
        return isInterface;
    }

    public void setInterface(boolean anInterface) {
        isInterface = anInterface;
    }

    public Method getMethod(String methodName) {
        Optional<Method> methodOptional = activeMethods.stream()
                .filter(method -> method.getMethodName().equals(methodName))
                .findFirst();
        return methodOptional.isPresent() ? methodOptional.get() : null;
    }

    public static class Method {
        private String methodName;
        private String desc;
        private Set<MethodInvokerBean> methodCallers;
        private Set<FieldInvokerBean> fieldCallers;
        private boolean visited = false;

        public Method(String methodName, String desc) {
            this.methodName = methodName;
            this.desc = desc;
        }

        public Method(String methodName, String desc, Set<MethodInvokerBean> methodCallers) {
            this.desc = desc;
            this.methodName = methodName;
            this.methodCallers = methodCallers;
        }

        public Method(String methodName, String desc, Set<MethodInvokerBean> methodCallers, Set<FieldInvokerBean> fieldCallers) {
            this.desc = desc;
            this.methodName = methodName;
            this.methodCallers = methodCallers;
            this.fieldCallers = fieldCallers;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getMethodName() {
            return methodName;
        }

        public void setMethodName(String methodName) {
            this.methodName = methodName;
        }

        public Set<MethodInvokerBean> getMethodCallers() {
            return methodCallers;
        }

        public Set<FieldInvokerBean> getFieldCallers() {
            return fieldCallers;
        }

        public boolean isVisited() {
            return visited;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Method)) return false;

            Method method = (Method) o;

            return getMethodName() != null ? getMethodName().equals(method.getMethodName())
                    : method.getMethodName() == null && (getDesc() != null ? getDesc().equals(method.getDesc())
                    : method.getDesc() == null);
        }

        @Override
        public int hashCode() {
            int result = getMethodName() != null ? getMethodName().hashCode() : 0;
            result = 31 * result + (getDesc() != null ? getDesc().hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Method{" +
                    "desc='" + desc + '\'' +
                    ", methodName='" + methodName + '\'' +
                    '}';
        }
    }

    public static class Field {
        private String fieldName;
        private String desc;

        public Field(String methodName, String desc) {
            this.desc = desc;
            this.fieldName = methodName;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Field)) return false;

            Field field = (Field) o;

            return getFieldName() != null ? getFieldName().equals(field.getFieldName()) : field.getFieldName() == null && (getDesc() != null ? getDesc().equals(field.getDesc()) : field.getDesc() == null);

        }

        @Override
        public int hashCode() {
            int result = getFieldName() != null ? getFieldName().hashCode() : 0;
            result = 31 * result + (getDesc() != null ? getDesc().hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Field{" +
                    "desc='" + desc + '\'' +
                    ", fieldName='" + fieldName + '\'' +
                    '}';
        }
    }
}
