package com.aj.tools.extractor.constant;

import java.util.regex.Pattern;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public interface ServiceExtractorConstants {
    String FILE_DIR = "file:";
    String SEPARATE = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
    String SPACE = " (?=([^\"]*\"[^\"]*\")*[^\"]*$)";
    Pattern JAVA_IMPORT_PATTERN = Pattern.compile("(?=\\p{javaWhitespace}*)import (.*);");
    Pattern LAMBDA_METHOD_PATTERN = Pattern.compile("lambda\\$.*\\$\\d*");
    String UTF_8 = "UTF-8";
    char DOT = '.';
    char FORWARD_SLASH = '/';
    String CLASS_EXT = ".class";
    String CLASS = "class";
    String BEAN = "bean";
    String JAVA_EXT = ".java";
    String IMPORT = "import";
    String STATIC = "static";
}
