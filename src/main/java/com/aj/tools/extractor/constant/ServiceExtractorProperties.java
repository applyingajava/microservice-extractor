package com.aj.tools.extractor.constant;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class ServiceExtractorProperties {
    public static String SOURCE_DIR = "D://andPro//SoapUISources//soapui-4.0.1-src//soapui-4.0.1//src//java";
    public static String SOURCE_CLASSES_DIR = "C:/Users/havem1/git/fulfillment-node-picking-manager/target/classes/";
    public static String DEST_SRC_DIR = "./destSrc";
    public static String DEST_TEMP_SRC_DIR = DEST_SRC_DIR + "Temp";
    public static String DEST_LIB_DIR = "./destLib";
    public static String DEST_SPRING_CNF_DIR = "./destSpringConfig";
    public static int DEPTH_LEVEL = 25;
    public static String EXTRACT_SOURCE_CLASSES_LIST;
    public static String ROOT_PACKAGE = "com.aj-hash.ecommerce.fulfillment.node";
    public static String SPRING_CONFIG_PATH = "com.aj-hash.ecommerce.fulfillment.node";

}