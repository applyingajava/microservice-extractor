package com.aj.tools.extractor.constant;

import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class ServiceExtractorPropertiesLoader {
    private static final Logger LOGGER = Logger.getLogger(ServiceExtractorPropertiesLoader.class.getName());

    private ServiceExtractorPropertiesLoader() {
    }

    public static void loadProperties() {
        LOGGER.info("Initializing properties.");

        Properties properties = new Properties();
        try {
            properties.load(ServiceExtractorPropertiesLoader.class.getClassLoader()
                    .getResourceAsStream("application.properties"));

            String sourceDir = (String) properties.get("source.dir");
            String destSourceDir = (String) properties.get("dest.src.dir");
            String destLibDir = (String) properties.get("dest.lib.dir");
            String maxIteration = (String) properties.get("max.iteration");

            String sourceClassesDir = (String) properties
                    .get("source.classes.dir");
            String extractSourceClassesList = (String) properties
                    .get("extract.source.classes.list");
            String sourceRootPackage = (String) properties
                    .get("source.root.package");
            String springConfigPath = (String) properties
                    .get("spring.config.path");

            assert StringUtils.isNoneEmpty(sourceDir);
            assert StringUtils.isNoneEmpty(destSourceDir);
            assert StringUtils.isNoneEmpty(destLibDir);
            assert StringUtils.isNoneEmpty(maxIteration);
            assert StringUtils.isNoneEmpty(sourceClassesDir);
            assert StringUtils.isNoneEmpty(extractSourceClassesList);
            assert StringUtils.isNoneEmpty(sourceRootPackage);
            assert StringUtils.isNoneEmpty(springConfigPath);

            ServiceExtractorProperties.SOURCE_DIR = sourceDir;
            ServiceExtractorProperties.DEST_SRC_DIR = destSourceDir;
            ServiceExtractorProperties.DEST_LIB_DIR = destLibDir;
            ServiceExtractorProperties.DEPTH_LEVEL = Integer.parseInt(maxIteration);
            ServiceExtractorProperties.SOURCE_CLASSES_DIR = sourceClassesDir;
            ServiceExtractorProperties.EXTRACT_SOURCE_CLASSES_LIST = extractSourceClassesList;
            ServiceExtractorProperties.ROOT_PACKAGE = sourceRootPackage;
            ServiceExtractorProperties.SPRING_CONFIG_PATH = springConfigPath;

            properties.forEach((key, value) -> LOGGER.info(key + "->"
                    + value));

        } catch (FileNotFoundException e) {
            LOGGER.severe(e.getMessage());
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }
}
