package com.aj.tools.extractor.factory;

import com.aj.tools.extractor.classloader.JavaFileClassLoader;
import com.aj.tools.extractor.core.handler.impl.ClassPathParser;
import com.aj.tools.extractor.core.handler.impl.DependentJarParser;
import com.aj.tools.extractor.core.handler.impl.SpringConfigBeanParser;
import com.aj.tools.extractor.core.handler.impl.StaticImportParser;
import com.aj.tools.extractor.processor.ClassOptimizerProcessor;
import com.aj.tools.extractor.processor.ClassTraversProcessor;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class BeanFactory {
    private BeanFactory() {
    }

    public static JavaFileClassLoader getFileClassLoader() {
        return JavaFileClassLoader.getInstance();
    }

    public static ClassOptimizerProcessor getClassOptimizerProcessor() {
        return ClassOptimizerProcessor.getInstance();
    }

    public static ClassTraversProcessor getClassTraversProcessor() {
        return ClassTraversProcessor.getInstance();
    }

    public static ClassPathParser getClassPathParser() {
        return ClassPathParser.getInstance();
    }

    public static DependentJarParser getDependentJarParser() {
        return DependentJarParser.getInstance();
    }

    public static StaticImportParser getStaticImportParser() {
        return StaticImportParser.getInstance();
    }

    public static SpringConfigBeanParser getSpringConfigBeanParser() {
        return SpringConfigBeanParser.getInstance();
    }

}
