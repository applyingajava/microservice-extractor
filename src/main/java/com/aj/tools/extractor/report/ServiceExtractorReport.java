package com.aj.tools.extractor.report;

import com.aj.tools.extractor.beans.JavaFile;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class ServiceExtractorReport {
    private ServiceExtractorReport() {
    }

    // working beans sets are the classes whose we need to find dependencies
    private static Set<JavaFile> workingJavaFileSet = new HashSet<>();
    // processed classes are the files -- those are copied and their
    // dependencies
    // have been found -- they are kept in the cache just not to repeat the
    // process again
    private static Set<JavaFile> processedJavaFileSet = new HashSet<>();
    // unprocessed beans set -- those are not found in the source,
    // may be found in the jar files
    private static Set<JavaFile> unProcessedJavaFileSet = new HashSet<>();
    // set the failed java beans into errorJavaFileSet
    private static Set<String> errorJavaFileSet = new HashSet<>();
    // to capture all used spring beans which are defined in spring config files
    private static Set<String> unUsedBeanSet = new HashSet<>();

    // counter
    private static int counter = 0;


    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        ServiceExtractorReport.counter = counter;
    }

    public static Set<String> getErrorJavaFileSet() {
        return errorJavaFileSet;
    }

    public static Set<JavaFile> getProcessedJavaFileSet() {
        return processedJavaFileSet;
    }

    public static Set<JavaFile> getUnProcessedJavaFileSet() {
        return unProcessedJavaFileSet;
    }

    public static Set<String> getUnUsedBeanSet() {
        return unUsedBeanSet;
    }

    public static Set<JavaFile> getWorkingJavaFileSet() {
        return workingJavaFileSet;
    }

    public static Set<String> getProcessedClasses() {
        return processedJavaFileSet.stream()
                .map(javaFile -> javaFile.getName().split("\\.")[0])
                .collect(Collectors.toSet());
    }
}
