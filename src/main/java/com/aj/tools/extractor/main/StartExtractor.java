package com.aj.tools.extractor.main;

import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.constant.ServiceExtractorPropertiesLoader;
import com.aj.tools.extractor.core.handler.spi.Parser;
import com.aj.tools.extractor.factory.BeanFactory;
import com.aj.tools.extractor.util.LoggerUtil;

import java.io.File;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class StartExtractor {
    private static final Logger LOGGER = Logger.getLogger(StartExtractor.class.getName());

    public static Parser[] parsers = {BeanFactory.getClassPathParser(),
            BeanFactory.getStaticImportParser(), BeanFactory.getDependentJarParser()};


    static {
        LoggerUtil.init();
    }

    public static void main(String[] args) {
        try {
            ServiceExtractorPropertiesLoader.loadProperties();
            createTempFolder();
            kickOffParsing();
            BeanFactory.getClassOptimizerProcessor().optimizeClasses();
            BeanFactory.getSpringConfigBeanParser().parse();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.exit(0);
    }


    static void kickOffParsing() throws Exception {
        LOGGER.info("Analysis Parsers have been kicked off.");
        Arrays.asList(parsers).stream().forEach(Parser::parse);
        LOGGER.info("Class and XML analysis has been done.");
    }

    static void createTempFolder() {
        LOGGER.info("Creating temporary directories to copy files.");
        File destSrcDir = new File(ServiceExtractorProperties.DEST_SRC_DIR);
        File destLibDir = new File(ServiceExtractorProperties.DEST_LIB_DIR);

        if (!destSrcDir.exists()) {
            destSrcDir.mkdir();
        }
        if (!destLibDir.exists()) {
            destLibDir.mkdir();
        }
    }
}
