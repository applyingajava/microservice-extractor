package com.aj.tools.extractor.util;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by Harshavardhan Reddy, Vemireddy on 2/26/2017.
 */
public class LoggerUtil {
    private static final Logger LOGGER = Logger.getLogger(LoggerUtil.class.getName());

    public static void init() {

        Handler consoleHandler = null;
        Handler fileHandler = null;
        Formatter simpleFormatter = null;

        try {
            //Creating consoleHandler and fileHandler
            consoleHandler = new ConsoleHandler();
            fileHandler = new FileHandler("./service-extractor-logging.log");
            // Creating SimpleFormatter
            simpleFormatter = new SimpleFormatter();
            //Assigning handlers to LOGGER object
            LOGGER.addHandler(consoleHandler);
            LOGGER.addHandler(fileHandler);
            // Setting formatter to the handler
            fileHandler.setFormatter(simpleFormatter);
            //Setting levels to handlers and LOGGER
            consoleHandler.setLevel(Level.ALL);
            fileHandler.setLevel(Level.ALL);
            LOGGER.setLevel(Level.ALL);
            LOGGER.config("Configuration done.");
            //Console handler removed
            LOGGER.removeHandler(consoleHandler);
            LOGGER.log(Level.FINE, "Finer logged");
        } catch (IOException exception) {
            LOGGER.log(Level.SEVERE, "Error occur in FileHandler.", exception);
        }
    }
}
