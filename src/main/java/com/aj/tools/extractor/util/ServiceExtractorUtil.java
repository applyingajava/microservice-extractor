package com.aj.tools.extractor.util;

import com.aj.tools.extractor.adapters.ClassInsRemapper;
import com.aj.tools.extractor.adapters.EmptyClassVisitor;
import com.aj.tools.extractor.adapters.MethodRemappingClassAdapter;
import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.constant.ServiceExtractorConstants;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.report.ServiceExtractorReport;
import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.Remapper;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static com.aj.tools.extractor.report.ServiceExtractorReport.getWorkingJavaFileSet;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class ServiceExtractorUtil {
    private static final Logger LOGGER = Logger.getLogger(ServiceExtractorUtil.class.getName());

    private ServiceExtractorUtil() {
    }

    private static Comparator<Class<?>> classNameComparator = (o1, o2) -> o1
            .getName().compareTo(o2.getName());


    public static Set<JavaFile> getUsedJavaFileList(String className) {
        Set<JavaFile> javaFileSet = new HashSet<>();
        // get the class name list
        List<String> classNameList = getUsedClasses(className);
        // now iterate the class name list
        classNameList.forEach(fullClassName -> addJavaFile(javaFileSet, fullClassName));
        // return the list
        return javaFileSet;
    }

    public static void addJavaFile(Set<JavaFile> javaFileSet, String fullClassName) {
        try {
            JavaFile javaFile = JavaFileUtil.getJavaFile(
                    ServiceExtractorProperties.SOURCE_DIR, fullClassName);
            javaFileSet.add(javaFile);
        } catch (Exception ex) {
            ex.toString();
        }
    }

    public static List<String> getUsedClasses(String name) {
        List<String> classNameList = new ArrayList<>();

        try {
            final Collection<Class<?>> classes = getClassesUsedBy(name, null);

            for (final Class<?> cls : classes) {
                String className = cls.getName();

                // if its not java own class
                if (!className.startsWith("java")) {
                    // if its not an array or inner class
                    if (!className.startsWith("[L") && !className.contains("$")) {
                        classNameList.add(className);
                    }
                    // if its some array
                    else if (className.startsWith("[L")) {
                        classNameList.add(className.substring(2,
                                className.length() - 1));
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }

        return classNameList;
    }

    public static String getImportableClassName(String className) {
        if (!className.startsWith("java")) {
            // if its not an array or inner class
            if (!className.startsWith("[L") && !className.contains("$")) {
                return className;
            }
            // if its some array
            else if (className.startsWith("[L")) {
                return className.substring(2, className.length() - 1);
            }
        }

        return null;

    }

    public static String getFullClassNameFromImport(String importString) {
        return importString.substring(7, importString.length() - 1).trim();
    }

    public static void buildWorkingJavaFile(String className)
            throws IOException {
        JavaFile sJavaFile = JavaFileUtil.getJavaFile(
                ServiceExtractorProperties.SOURCE_DIR, className);
        Set<JavaFile> usedJavaFileList = getUsedJavaFileList(className,
                sJavaFile);
        getWorkingJavaFileSet().addAll(usedJavaFileList);
    }

    public static Set<JavaFile> getUsedJavaFileList(String startingClassName,
                                                    JavaFile sJavaFile) throws IOException {
        String destDirPath = ServiceExtractorProperties.DEST_TEMP_SRC_DIR
                + File.separator + sJavaFile.getPackageDir();
        // now copy beans
        FileUtils.copyFileToDirectory(sJavaFile, new File(destDirPath));
        // get the dependencies of this class
        return ServiceExtractorUtil
                .getUsedJavaFileList(startingClassName);
    }

    public static Set<Class<?>> getClassesUsedBy(final String name,
                                                 final String prefix) throws IOException {
        ClassReader reader = ClassLoaderUtil.getClassReader(name);
        final Set<Class<?>> classesLoaded = new TreeSet<>(classNameComparator);
        final Remapper remapper = new ClassInsRemapper(classesLoaded, prefix, name);
        final ClassVisitor inner = new EmptyClassVisitor(Opcodes.ASM5);
        final MethodRemappingClassAdapter visitor = new MethodRemappingClassAdapter(
                inner, remapper);
        try {
            reader.accept(visitor, ClassReader.EXPAND_FRAMES);
        } catch (Exception ex) {
            ServiceExtractorReport.getErrorJavaFileSet().add(name);
            LOGGER.severe("Error occured while processing class : " + name + " details: " + ex);
        }
        return classesLoaded;
    }


    public static Stream<String> getExtractionClasses() {
        return Stream.of(
                ServiceExtractorProperties.EXTRACT_SOURCE_CLASSES_LIST
                        .split(ServiceExtractorConstants.SEPARATE));
    }

}
