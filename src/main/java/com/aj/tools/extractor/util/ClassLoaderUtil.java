package com.aj.tools.extractor.util;

import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import org.objectweb.asm.ClassReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import static com.aj.tools.extractor.report.ServiceExtractorReport.getErrorJavaFileSet;

/**
 * Created by harsha.vemireddy on 2/28/2017.
 */
public class ClassLoaderUtil {
    private static final Logger LOGGER = Logger.getLogger(ClassLoaderUtil.class.getName());
    private final static int MAX_RETRY_COUNT = 3;

    private ClassLoaderUtil() {
    }

    /**
     * load class from external directory
     *
     * @param name
     * @return
     * @throws IOException
     */
    public static ClassReader loadClass(final String name)
            throws IOException {
        String classPath = (ServiceExtractorProperties.SOURCE_CLASSES_DIR + name)
                .replace('.', '/') + ".class";
        InputStream resourceAsStream = new FileInputStream(new File(classPath));
        return new ClassReader(resourceAsStream);
    }


    /**
     * load class from external directory with retry logic
     *
     * @param name
     * @return
     * @throws IOException
     */
    public static ClassReader getClassReader(String name) throws IOException {
        ClassReader reader = null;
        int i = 0;
        while (i++ < MAX_RETRY_COUNT) {
            try {
                reader = loadClass(name);
                break;
            } catch (ArrayIndexOutOfBoundsException e) {
                LOGGER.warning(name + "Encounter ArrayIndexOutOfBoundsException, retrying class load logic.");
                getErrorJavaFileSet().add(name);
            }
        }
        return reader;
    }
}
