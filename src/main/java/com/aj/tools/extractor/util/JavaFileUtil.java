package com.aj.tools.extractor.util;

import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import org.w3c.dom.Document;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class JavaFileUtil {
    private JavaFileUtil() {
    }

    public static JavaFile getJavaFile(String SourceDir, String fullClassName) {
        String packageDir = fullClassName.replace(".", File.separator);
        String fullFilePath = SourceDir + File.separator + packageDir + ".java";
        packageDir = packageDir.substring(0, packageDir.lastIndexOf(File.separator));

        String dir = SourceDir + File.separator + packageDir;
        JavaFile javaFile = new JavaFile(fullFilePath);
        javaFile.setFullClassName(fullClassName);
        javaFile.setDirName(dir);
        javaFile.setPackageDir(packageDir);

        return javaFile;
    }


    public static void saveFile(Document document, String fileName)
            throws TransformerFactoryConfigurationError,
            TransformerConfigurationException, TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        String destDirPath = ServiceExtractorProperties.DEST_SPRING_CNF_DIR
                + File.separator + fileName;
        File file = new File(destDirPath);
        file.getParentFile().mkdirs();
        Result result = new StreamResult(file);
        Source source = new DOMSource(document);
        transformer.transform(source, result);
    }
}
