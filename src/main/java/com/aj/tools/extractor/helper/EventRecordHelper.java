package com.aj.tools.extractor.helper;

import com.aj.tools.extractor.beans.ClassFile;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class EventRecordHelper {
    private static final Logger LOGGER = Logger.getLogger(EventRecordHelper.class.getName());

    private static String currentClassName;
    private static boolean activeMethod;

    private EventRecordHelper() {
    }

    public static String getCurrentClassName() {
        return currentClassName;
    }

    public static void setCurrentClassName(String currentClassName) {
        EventRecordHelper.currentClassName = currentClassName;
        if (currentClassName.startsWith("[L")) {
            currentClassName = currentClassName.substring(2, currentClassName.length() - 1);
        }
        ClassVisitorCache.getClassHierarchy().put(currentClassName, new ClassFile(currentClassName));
    }

    public static ClassFile getCurrentClass() {
        return ClassVisitorCache.getClass(currentClassName);
    }

    public static ClassFile getCurrentClass(String className) {
        if (EventRecordHelper.currentClassName.equals(className)) {
            return getCurrentClass();
        }
        if (className.startsWith("[L")) {
            className = className.substring(2, className.length() - 1);
        }
        Map<String, ClassFile> dependentClasses = getCurrentClass().getDependentClasses();
        dependentClasses.putIfAbsent(className, new ClassFile(className));
        return dependentClasses.get(className);
    }

    public static boolean isActiveMethod() {
        return activeMethod;
    }

    public static void setActiveMethod(boolean activeMethod) {
        EventRecordHelper.activeMethod = activeMethod;
    }

}
