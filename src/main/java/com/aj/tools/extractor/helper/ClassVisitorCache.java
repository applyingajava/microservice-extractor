package com.aj.tools.extractor.helper;

import com.aj.tools.extractor.beans.ClassFile;
import com.aj.tools.extractor.core.handler.impl.StaticImportParser;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BinaryOperator;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class ClassVisitorCache {
    private static final Logger LOGGER = Logger.getLogger(ClassVisitorCache.class.getName());

    private final static Set<String> parsedClassNames = new TreeSet<>();
    //Bad class files inside your application, class loaders could able to load them into memory(java.lang.ArrayIndexOutOfBoundsException).
    private final static Set<String> errorClassNames = new TreeSet<>();
    private final static Map<String, ClassFile> classHierarchy = new ConcurrentHashMap<>();
    private final static Map<String, ClassFile> classHierarchyVisitor = new ConcurrentHashMap<>();
    private static final BinaryOperator<ClassFile> MAP_MERGE_OPS = (value1, value2) -> {
        value1.getActiveFields().addAll(value2.getActiveFields());
        value1.getActiveMethods().addAll(value2.getActiveMethods());
        return value1;
    };

    private ClassVisitorCache() {
    }

    public static Set<String> getParsedClassNames() {
        return parsedClassNames;
    }

    public static Map<String, ClassFile> getClassHierarchy() {
        return classHierarchy;
    }

    public static Map<String, ClassFile> getClassHierarchyVisitor() {
        return classHierarchyVisitor;
    }

    public static ClassFile getClass(String key) {
        return classHierarchy.get(key);
    }

    public static Set<String> getErrorClassNames() {
        return errorClassNames;
    }

    public static Map<String, ClassFile> consolidateCache() {
        //collect all dependent classes
        Map<String, ClassFile> classFileMap = classHierarchy.entrySet().stream()
                .map(classCache -> classCache.getValue().getDependentClasses().entrySet())
                .flatMap(Collection::stream).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, MAP_MERGE_OPS));

        //consolidate all parent/direct classes
        Map<String, ClassFile> parentClassFileMap = classHierarchy.entrySet().stream()
                .map(classCache -> classCache)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, MAP_MERGE_OPS));

        //merge both parent and dependent classes
        return Stream.of(parentClassFileMap, classFileMap).map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, MAP_MERGE_OPS));
    }

    public static Set<String> getProcessedClassNames() {
        Set<String> processedClasses = ClassVisitorCache.getClassHierarchyVisitor().entrySet().stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
        processedClasses.addAll(ClassVisitorCache.getErrorClassNames());
        return processedClasses;
    }


}
