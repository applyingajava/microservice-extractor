package com.aj.tools.extractor.helper;

import com.aj.tools.extractor.constant.ServiceExtractorConstants;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.aj.tools.extractor.constant.ServiceExtractorProperties.SOURCE_CLASSES_DIR;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class ReflectionsHelper {
    private static final Logger LOGGER = Logger.getLogger(ReflectionsHelper.class.getName());

    private static final String SUB_TYPES_SCANNER = "SubTypesScanner";
    private static Reflections reflections;

    static {
        try {
            reflections = new Reflections(
                    new ConfigurationBuilder().setUrls(Arrays.asList(new URL(ServiceExtractorConstants.FILE_DIR
                            + SOURCE_CLASSES_DIR))));
        } catch (MalformedURLException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private ReflectionsHelper() {
    }

    public static Set<String> getSubClasses(final Class<?> myObjectClass) {
        Iterable<String> iterable = reflections.getStore().get(
                SUB_TYPES_SCANNER, myObjectClass.getName());
        return iterable != null ? stream(iterable).collect(Collectors.toSet())
                : Collections.emptySet();

    }

    private static <T> Stream<T> stream(Iterable<T> iterable) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                iterable.iterator(), Spliterator.ORDERED), false);
    }
}
