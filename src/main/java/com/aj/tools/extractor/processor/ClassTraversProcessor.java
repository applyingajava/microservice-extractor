package com.aj.tools.extractor.processor;

import com.aj.tools.extractor.beans.ClassFile;
import com.aj.tools.extractor.beans.FieldInvokerBean;
import com.aj.tools.extractor.beans.MethodInvokerBean;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.helper.ClassVisitorCache;
import com.aj.tools.extractor.helper.ReflectionsHelper;
import com.aj.tools.extractor.factory.BeanFactory;
import com.aj.tools.extractor.util.ServiceExtractorUtil;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by harsha.vemireddy on 2/24/2017.
 */
public class ClassTraversProcessor {
    private static final Logger LOGGER = Logger.getLogger(ClassTraversProcessor.class.getName());

    //singleton instance
    private static ClassTraversProcessor instance = new ClassTraversProcessor();
    //map to hold optimized class hierarchy
    private static final Map<String, ClassFile> classHierarchyVisitor = ClassVisitorCache.getClassHierarchyVisitor();
    //getting class hierarchy of all the classed which have been loaded by parsers
    private static final Map<String, ClassFile> classHierarchy = ClassVisitorCache.getClassHierarchy();

    protected ClassTraversProcessor() {
        // Exists only to defeat instantiation.
    }

    public static ClassTraversProcessor getInstance() {
        if (instance == null) {
            instance = new ClassTraversProcessor();
        }
        return instance;
    }

    public Map<String, ClassFile> processClassHierarchy() {
        final Queue<ClassFile.Method> methodQueue = new LinkedList<>();
        //if the class hierarchy is not empty return existing map instead of recomputing again
        if (!classHierarchyVisitor.isEmpty()) {
            return ClassVisitorCache.getClassHierarchyVisitor();
        }
        //traverse through all the extraction services and add their methods to method queue,
        //each method will be picked up and process their methods recursively until it hits end of call hierarchy
        Stream<String> pathClasses = ServiceExtractorUtil.getExtractionClasses();
        pathClasses.forEach(
                startingClassName -> initializeMethodCallStack(methodQueue, startingClassName));

        //get methods being invoked from services and add them into queue to forward track all its method invocation
        while (!methodQueue.isEmpty()) {
            ClassFile.Method recentMethod = methodQueue.remove();
            //track and record the active class fields
            recordFieldCallStack(recentMethod);
            //track and record the active methods
            recordMethodCallStack(methodQueue, recentMethod);
        }
        return classHierarchyVisitor;
    }

    /**
     * add extraction service method calls to method queue
     *
     * @param methodQueue
     * @param startingClassName
     */
    private void initializeMethodCallStack(Queue<ClassFile.Method> methodQueue, String startingClassName) {
        ClassFile classFile = classHierarchy.get(startingClassName);
        String className = classFile.getClassName();
        if (className.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
            classHierarchyVisitor.put(className, generateExtractionClassFile(classFile));
        }
        methodQueue.addAll(classFile.getActiveMethods());
    }

    /**
     * traverse through all the extraction services and add their methods to method queue,
     * each method will be picked up and process their methods recursively until it hits end of call hierarchy
     *
     * @param classFile
     * @return
     */
    private ClassFile generateExtractionClassFile(ClassFile classFile) {
        ClassFile rootClass = new ClassFile(classFile.getClassName());
        rootClass.getActiveFields().addAll(classFile.getActiveFields());
        Set<ClassFile.Method> activeMethods = classFile.getActiveMethods();
        Set<ClassFile.Method> methodSet = activeMethods.stream()
                .map(method -> new ClassFile.Method(method.getMethodName(), method.getDesc()))
                .collect(Collectors.toSet());
        rootClass.getActiveMethods().addAll(methodSet);
        return rootClass;
    }

    /**
     * track and record the active methods
     *
     * @param methodQueue
     * @param recentMethod
     */
    private void recordMethodCallStack(Queue<ClassFile.Method> methodQueue, ClassFile.Method recentMethod) {
        Set<MethodInvokerBean> methodCallers = recentMethod.getMethodCallers();
        //get the each method's method stack frame and add it to method queue for processing
        methodCallers.stream().forEach(method -> {
            String className = method.getClassName();
            //filter classes by extraction module's root package to avoid processing
            // all the external classes which are part of externals dependent jars
            if (className.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
                Class<?> myObjectClass = null;
                try {
                    //check for interface
                    myObjectClass = BeanFactory.getFileClassLoader().loadClass(className);
                    if (myObjectClass.isInterface()) {
                        Set<String> subClasses = ReflectionsHelper.getSubClasses(myObjectClass);
                        //since most of the method calls wil be triggered through interface reference,
                        // finding all the implementors of interface and parse their methods
                        subClasses.stream().forEach(czName -> processMethodCallStack(methodQueue, method, czName));
                        //Add interface too in the processed class list
                        recordActiveMethods(method, className);
                        ClassFile classFile = classHierarchyVisitor.get(className);
                        //setting interface flag for optimization
                        classFile.setInterface(true);
                        //add subclass methods and variables to interface so that subclasses adheres contract
                    } else {
                        processMethodCallStack(methodQueue, method, className);
                        Class<?> superclass = myObjectClass.getSuperclass();
                        recordActiveMethodsForClasses(method, superclass);
                        Class<?>[] interfaces = myObjectClass.getInterfaces();
                        for (Class<?> $interface : interfaces) {
                            recordActiveMethodsForInterfaces(method, $interface);
                        }
                    }

                } catch (ClassNotFoundException e) {
                    LOGGER.severe(e.getMessage());
                }
            }
        });
    }

    /**
     * recording active methods for optimization
     *
     * @param method
     * @param superclass
     */
    private void recordActiveMethodsForClasses(MethodInvokerBean method, Class<?> superclass) {
        String superclassName = superclass.getName();
        //filter classes by extraction module's root package to avoid processing
        // all the external classes which are part of externals dependent jars
        if (superclassName.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
            if (ClassVisitorCache.getProcessedClassNames().contains(superclassName)) {
                recordActiveMethods(method, superclassName);
            } else {
                //if the interface or superclass of any processed class is not present
                // then add it to error class list then it would be added to extracting module
                ClassVisitorCache.getErrorClassNames().add(superclassName);
            }
        }
    }

    /**
     * recording active methods for optimization
     *
     * @param method
     * @param superclass
     */
    private void recordActiveMethodsForInterfaces(MethodInvokerBean method, Class<?> superclass) {
        recordActiveMethodsForClasses(method, superclass);
    }

    private void processMethodCallStack(Queue<ClassFile.Method> methodQueue, MethodInvokerBean method, String czName) {
        recordActiveMethods(method, czName);
        updateMethodCallStack(methodQueue, method, czName);
    }

    /**
     *
     * @param methodQueue
     * @param method
     * @param czName
     */
    private void updateMethodCallStack(Queue<ClassFile.Method> methodQueue, MethodInvokerBean method, String czName) {
        ClassFile $classFileSub = classHierarchy.get(czName);
        //if the class's byte code is not compatible it wouldn't have loaded while analysing byte code
        // so adding it to the error class list so it would be copied into the extracted module
        if ($classFileSub.getActiveMethods().isEmpty() && $classFileSub.getActiveMethods().isEmpty()) {
            ClassVisitorCache.getErrorClassNames().add(czName);
        } else {
            ClassFile.Method $classFileSubMethod = $classFileSub.getMethod(method.getMethodName());
            //marking class as visited so it would not be visited again
            if (Objects.nonNull($classFileSubMethod) && !$classFileSubMethod.isVisited()) {
                methodQueue.add($classFileSubMethod);
                $classFileSubMethod.setVisited(true);
            }
        }
    }

    /**
     * recording active methods for optimization
     *
     * @param method
     * @param czName
     */
    private void recordActiveMethods(MethodInvokerBean method, String czName) {
        classHierarchyVisitor.putIfAbsent(czName, new ClassFile(czName));
        ClassFile $czName = classHierarchyVisitor.get(czName);
        $czName.getActiveMethods().add(new ClassFile.Method(method.getMethodName(), method.getMethodDesc()));
    }

    /**
     * track and record the active class fields
     *
     * @param recentMethod
     */
    private void recordFieldCallStack(ClassFile.Method recentMethod) {
        Set<FieldInvokerBean> fieldCallers = recentMethod.getFieldCallers();
        fieldCallers.stream().forEach(this::recordActiveFields);
    }

    /**
     * recording active fields for optimization
     *
     * @param field
     */
    private void recordActiveFields(FieldInvokerBean field) {
        String className = field.getClassName();
        //filter classes by extraction module's root package to avoid processing
        // all the external classes which are part of externals dependent jars
        if (className.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
            classHierarchyVisitor.putIfAbsent(className, new ClassFile(className));
            ClassFile classFile = classHierarchyVisitor.get(className);
            classFile.getActiveFields().add(new ClassFile.Field(field.getFieldName(), field.getFieldDesc()));
        }
    }

    public static Map<String, ClassFile> getClassHierarchyVisitor() {
        return BeanFactory.getClassTraversProcessor().processClassHierarchy();
    }
}
