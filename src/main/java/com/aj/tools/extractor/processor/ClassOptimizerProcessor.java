package com.aj.tools.extractor.processor;

import com.aj.tools.extractor.beans.ClassFile;
import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.factory.BeanFactory;
import com.aj.tools.extractor.helper.ReflectionsHelper;
import com.aj.tools.extractor.report.ServiceExtractorReport;
import com.aj.tools.extractor.util.JavaFileUtil;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.Name;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by harsha.vemireddy on 2/25/2017.
 */
public class ClassOptimizerProcessor {
    private static final Logger LOGGER = Logger.getLogger(ClassOptimizerProcessor.class.getName());

    //singleton instance
    private static ClassOptimizerProcessor instance = new ClassOptimizerProcessor();
    //map to hold optimized class hierarchy
    private static Map<String, ClassFile> classFileMap;
    private static Set<String> activeClasses;
    private static Set<String> activeClassNamees;

    protected ClassOptimizerProcessor() {
        // Exists only to defeat instantiation.
    }

    public static ClassOptimizerProcessor getInstance() {
        if (instance == null) {
            instance = new ClassOptimizerProcessor();
        }
        return instance;
    }

    /**
     * this method walks though all the active classes and removes unused/dead code
     */
    public void optimizeClasses() {
        LOGGER.info("Class Optimizer Processing started....");
        classFileMap = ClassTraversProcessor.getClassHierarchyVisitor();
        populateActiveClasses();
        populateActiveClassNames();

        //travers through all the active classes for dead code removal
        classFileMap.entrySet().stream().forEach(entry -> {
            String className = entry.getKey();
            ClassFile classFile = entry.getValue();
            Class<?> myObjectClass = null;
            try {
                myObjectClass = BeanFactory.getFileClassLoader()
                        .loadClass(className);
                Set<String> subClasses = ReflectionsHelper.getSubClasses(myObjectClass);
                //add subclass methods and variables to interface so that subclasses adheres contract
                subClasses.stream().forEach(czName -> {
                    ClassFile $czName = classFileMap.get(czName);
                    classFile.getActiveMethods().addAll($czName.getActiveMethods());
                    classFile.getActiveFields().addAll($czName.getActiveFields());
                });
            } catch (Exception e) {
                LOGGER.severe(e.getMessage());
            }

            //get the java file which was hosted by ClassTraversProcessor in the temp folder
            JavaFile sJavaFile = JavaFileUtil.getJavaFile(ServiceExtractorProperties.DEST_TEMP_SRC_DIR, className);
            try (FileInputStream in = new FileInputStream(sJavaFile.getFullFilePath())) {
                //load java file into CompilationUnit
                CompilationUnit cu = JavaParser.parse(in);
                String destDirPath = ServiceExtractorProperties.DEST_SRC_DIR
                        + File.separator + sJavaFile.getPackageDir() + File.separator + sJavaFile.getName();
                File file = new File(destDirPath);
                file.getParentFile().mkdirs();
                try {
                    //creating instance of ClassOptimizerVisitor for optimizing java code
                    new ClassOptimizerProcessor.ClassOptimizerVisitor(classFile, myObjectClass).visit(cu, null);
                    FileUtils.writeStringToFile(file, cu.toString());
                } catch (Throwable e) {
                    //if any abnormal exception thrown by ClassOptimizerVisitor while parsing a java file
                    // would be caught here and will not perform optimization for those classes
                    LOGGER.warning("Error occurred while optimizing Class File, So Replacing with the Original :" + className);
                    FileUtils.copyFile(sJavaFile, file);
                }
            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
            }
        });
        LOGGER.info("Class Optimization has been done and files were copied into destination directories.");
    }

    /**
     * ClassOptimizerVisitor will parse through java file for removal of dead code
     */
    private static class ClassOptimizerVisitor extends ModifierVisitor<Void> {
        private ClassFile classFile;
        Class<?> myObjectClass;
        private Set<String> importClassNames;
        private Set<String> importClassNameOnly;

        public ClassOptimizerVisitor(ClassFile classFile) {
            this.classFile = classFile;
            importClassNames = new TreeSet<>();
        }

        public ClassOptimizerVisitor(ClassFile classFile, Class<?> myObjectClass) {
            this.classFile = classFile;
            this.myObjectClass = myObjectClass;
            importClassNames = new TreeSet<>();
        }

        /**
         * visit methods of java file to identifying unused code and delete it
         *
         * @param method
         * @param arg
         * @return
         */
        @Override
        public Node visit(MethodDeclaration method, Void arg) {
            String methodName = method.getName().getIdentifier();

            //if the method is present in any of the interfaces, don't delete it.
            if (Objects.nonNull(myObjectClass) &&
                    (Objects.nonNull(myObjectClass.getInterfaces()) && myObjectClass.getInterfaces().length > 0)) {
                Optional<ClassFile.Method> methodOptional = Stream.of(myObjectClass.getInterfaces())
                        .map($interface -> classFileMap.get($interface.getName()).getActiveMethods())
                        .flatMap(Set::stream)
                        .filter($method -> $method.getMethodName().equals(methodName))
                        .findFirst();
                if (methodOptional.isPresent()) {
                    return method;
                }
            }
            Set<String> methodSet = classFile.getActiveFields().stream().flatMap(field -> {
                String capitalize = StringUtils.capitalize(field.getFieldName());
                return Stream.of(
                        "get" + capitalize, "set" + capitalize);
            }).collect(Collectors.toSet());
            if (methodSet.contains(methodName)) {
                return method;
            }


            classFile.getActiveFields();
            //remove if method doesn't present in the active method list
            Optional<ClassFile.Method> methodOptional = classFile.getActiveMethods()
                    .stream()
                    .filter($method -> $method.getMethodName().equals(methodName))
                    .findFirst();
            if (!methodOptional.isPresent()) {
                return null;
            }
            return method;
        }

        /**
         * visit fields of java file to identifying unused code and delete it
         *
         * @param field
         * @param arg
         * @return
         */
        @Override
        public Node visit(FieldDeclaration field, Void arg) {
            //check if it is an interface
            if (classFile.isInterface()) {
                return field;
            }
            NodeList<VariableDeclarator> variables = field.getVariables();
            VariableDeclarator variableDeclarator = variables.stream().findFirst().get();
            if (variableDeclarator != null) {
                String fieldId = variableDeclarator.getName().getId();
                String dataType = variableDeclarator.getType().toString();
                Optional<Modifier> modifierOptional = field.getModifiers()
                        .stream().filter(modifier -> modifier.equals(Modifier.PRIVATE))
                        .findFirst();
                //if an import is a static check for private fields
                if (modifierOptional.isPresent()) {
                    if (!getClassActiveFieldNames().contains(dataType)
                            && ServiceExtractorReport.getProcessedClasses().contains(dataType)) {
                        //remove import if it doesn't match any of above cases
                        return null;
                    }
                    return field;
                }
                Optional<ClassFile.Field> fieldOptional = getClassActiveField(fieldId);
                if (!fieldOptional.isPresent()) {
                    return null;
                }
            }
            return field;
        }

        /**
         * visit Enums of java file to identifying unused code and delete it
         *
         * @param n
         * @param arg
         * @return
         */
        @Override
        public Visitable visit(EnumConstantDeclaration n, Void arg) {
            String identifier = n.getName().getIdentifier();
            Optional<ClassFile.Field> fieldOptional = getClassActiveField(identifier);
            if (!fieldOptional.isPresent()) {
                return null;
            }
            return n;
        }

        /**
         * visit imports of java file to identifying unused code and delete it
         *
         * @param n
         * @param arg
         * @return
         */
        @Override
        public Node visit(ImportDeclaration n, Void arg) {
            Name name = n.getName();
            String identifier = name.getIdentifier();
            String importStmt = null;
            //check if am import is static
            boolean staticImport = n.isStatic();
            if (staticImport) {
                String className = null;
                //get class name for static import to check whether class has the importClassNames field as active in it.
                Name qualifier = name.getQualifier().get();
                if (Objects.nonNull(qualifier)) {
                    className = qualifier.toString();
                    importStmt = className;
                }
                //check whether import is part of working project
                if (className.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
                    ClassFile classFile = classFileMap.get(className);
                    if (classFile != null) {
                        Optional<ClassFile.Field> fieldOptional = getClassActiveField(classFile, identifier);
                        //remove import if it's not a active field of class
                        if (!fieldOptional.isPresent()) {
                            return null;
                        }
                    } else {
                        //remove import if the class is not present
                        return null;
                    }
                }
            } else {
                identifier = name.toString();
                importStmt = identifier;
                if (identifier.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
                    if (activeClasses.contains(identifier)) {
                        importClassNames.add(identifier);
                        return n;
                    } else {
                        return null;
                    }
                }
            }
            importClassNames.add(importStmt);
            return n;
        }

        public Optional<ClassFile.Field> getClassActiveField(String identifier) {
            return classFile.getActiveFields()
                    .stream()
                    .filter($field -> $field.getFieldName().equals(identifier))
                    .findFirst();
        }

        public Optional<ClassFile.Field> getClassActiveField(ClassFile classFile, String identifier) {
            return classFile.getActiveFields()
                    .stream()
                    .filter($field -> $field.getFieldName().equals(identifier))
                    .findFirst();
        }

        public Set<String> getImportClassNameOnly() {
            if (importClassNameOnly != null) {
                return importClassNameOnly;
            }
            return importClassNames.stream()
                    .filter(className -> className.contains(ServiceExtractorProperties.ROOT_PACKAGE))
                    .map(className -> Stream.of(className.split("\\."))
                            .reduce((cls1, cls2) -> cls2).orElse(null))
                    .collect(Collectors.toSet());
        }

        public Set<String> getClassActiveFields() {
            return classFile.getActiveFields().stream().map(this::getFieldType).collect(Collectors.toSet());
        }


        public Set<String> getClassActiveFieldNames() {
            return classFile.getActiveFields().stream()
                    .map(this::getFieldType)
                    .map(className -> Stream.of(className.split("\\."))
                            .reduce((cls1, cls2) -> cls2).orElse(null))
                    .collect(Collectors.toSet());
        }

        public String getFieldType(ClassFile.Field field) {
            String desc = StringUtils.replace(field.getDesc(), "/", ".");
            if (desc.startsWith("L")) {
                desc = desc.substring(1, desc.length() - 1);
            }
            return desc;
        }
    }

    private static void populateActiveClasses() {
        activeClasses = classFileMap.entrySet().stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    private static void populateActiveClassNames() {
        activeClassNamees = activeClasses.stream()
                .map(className -> Stream.of(className.split(".")).reduce((n1, n2) -> n2).orElse(null))
                .collect(Collectors.toSet());
    }

    public static boolean isFieldPresent(String fieldName) {
        return activeClasses.contains(fieldName);
    }

}
