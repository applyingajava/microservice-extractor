package com.aj.tools.extractor.classloader;

import com.aj.tools.extractor.adapters.MethodRemappingClassAdapter;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

import static com.aj.tools.extractor.constant.ServiceExtractorConstants.*;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public final class JavaFileClassLoader extends ClassLoader {
    private static final Logger LOGGER = Logger.getLogger(JavaFileClassLoader.class.getName());

    private static final JavaFileClassLoader instance = new JavaFileClassLoader();

    protected JavaFileClassLoader() {
        super(JavaFileClassLoader.class.getClassLoader());
    }

    public static JavaFileClassLoader getInstance() {
        return instance;
    }

    public Class<?> loadClass(String name) throws ClassNotFoundException {
        try {
            if (!name.contains(ServiceExtractorProperties.ROOT_PACKAGE)) {
                return super.loadClass(name);
            }
            return loadClass(name, ServiceExtractorProperties.SOURCE_CLASSES_DIR);
        } catch (Exception e) {
            LOGGER.severe("Error occured while processing class : " + name + " details: " + e);
        }
        return super.loadClass(name);
    }

    private Class<?> loadClass(String name, String url)
            throws IOException, ClassFormatError {
        if (name.startsWith("[L")) {
            name = name.substring(2, name.length() - 1);
        }

        String classPath = FILE_DIR
                + (url + name).replace(DOT, FORWARD_SLASH)
                + CLASS_EXT;

        URL myUrl = new URL(classPath);
        URLConnection connection = myUrl.openConnection();
        InputStream input = connection.getInputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int data = input.read();

        while (data != -1) {
            buffer.write(data);
            data = input.read();
        }
        input.close();

        byte[] classData = buffer.toByteArray();
        Class<?> findClass = findLoadedClass(name);
        if (findClass != null) {
            return findClass;
        }
        return defineClass(name, classData, 0, classData.length);
    }

    @Override
    public Class<?> findClass(String s) throws ClassNotFoundException {
        return super.findClass(s);
    }
}
