package com.aj.tools.extractor.core.handler.impl;

import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.classloader.JavaFileClassLoader;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.core.handler.spi.Parser;
import com.aj.tools.extractor.util.JavaFileUtil;
import com.aj.tools.extractor.util.ServiceExtractorUtil;

import java.io.IOException;
import java.util.Set;
import java.util.logging.Logger;

import static com.aj.tools.extractor.report.ServiceExtractorReport.*;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class ClassPathParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(ClassPathParser.class.getName());

    private static ClassPathParser instance = new ClassPathParser();

    protected ClassPathParser() {
        // Exists only to defeat instantiation.
    }

    public static ClassPathParser getInstance() {
        if (instance == null) {
            instance = new ClassPathParser();
        }
        return instance;
    }

    @Override
    public void parse() {
        try {
            buildClassList();
            setCounter(Parser.process(getCounter()));
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private void buildClassList() {
        ServiceExtractorUtil.getExtractionClasses().forEach(
                this::buildWorkingJavaFile);
    }

    private void buildWorkingJavaFile(String className) {
        try {
            JavaFile sJavaFile = JavaFileUtil.getJavaFile(
                    ServiceExtractorProperties
                            .SOURCE_DIR, className);
            Set<JavaFile> usedJavaFileList = ServiceExtractorUtil.getUsedJavaFileList(
                    className, sJavaFile);
            getWorkingJavaFileSet().addAll(usedJavaFileList);
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

}
