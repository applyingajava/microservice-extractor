package com.aj.tools.extractor.core.handler.impl;

import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.core.handler.spi.Parser;
import com.aj.tools.extractor.helper.ClassVisitorCache;
import com.aj.tools.extractor.util.JavaFileUtil;
import org.joox.Filter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.aj.tools.extractor.constant.ServiceExtractorConstants.BEAN;
import static com.aj.tools.extractor.constant.ServiceExtractorConstants.CLASS;
import static com.aj.tools.extractor.report.ServiceExtractorReport.getUnUsedBeanSet;
import static org.joox.JOOX.$;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class SpringConfigBeanParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(SpringConfigBeanParser.class.getName());

    private static Map<String, String> classIdMap = new HashMap<>();
    private static SpringConfigBeanParser instance = new SpringConfigBeanParser();

    protected SpringConfigBeanParser() {
        // Exists only to defeat instantiation.
    }

    public static SpringConfigBeanParser getInstance() {
        if (instance == null) {
            instance = new SpringConfigBeanParser();
        }
        return instance;
    }

    @Override
    public void parse() {
        LOGGER.info("SpringConfigBean Parsing started....");
        try {
            readSpringConfigAndRemoveUnUsedBeans(new File(ServiceExtractorProperties.SPRING_CONFIG_PATH));
            readSpringConfigAndRemoveUnUsedProperties(new File(ServiceExtractorProperties.DEST_SPRING_CNF_DIR));
        } catch (SAXException | IOException e) {
            LOGGER.severe(e.getMessage());
        }
        LOGGER.info("SpringConfigBean Parsing is Done.");

    }

    private void readSpringConfigAndRemoveUnUsedBeans(File directory) throws SAXException,
            IOException {
        Set<String> processedClasses = getProcessedClassNames();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc = null;
        for (File file : directory.listFiles()) {
            try {
                if (file.isDirectory()) {
                    readSpringConfigAndRemoveUnUsedBeans(file);
                }
                dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(file);
                Set<String> unUsedBeans = $(doc).find(BEAN)
                        .map(bean -> {
                            String $class = $(bean).attr(CLASS).toString();
                            String idAttr = $(bean).attr("id");
                            if (Objects.nonNull(idAttr) && ($class.contains(ServiceExtractorProperties.ROOT_PACKAGE)
                                    && !processedClasses.contains($class))) {
                                String id = idAttr.toString();
                                classIdMap.put(id, $class);
                            }
                            return $class;
                        }).stream()
                        .filter(beanName -> !processedClasses.contains(beanName))
                        .collect(Collectors.toSet());
                getUnUsedBeanSet().addAll(unUsedBeans);

                Document document = $(doc).document();
                $(document).find(BEAN).filter(unUsedBeanFilter(unUsedBeans))
                        .remove();
                JavaFileUtil.saveFile(document, file.getName());
            } catch (ParserConfigurationException e) {
                LOGGER.severe(e.getMessage());
            } catch (TransformerFactoryConfigurationError
                    | TransformerException e) {
                LOGGER.severe(e.getMessage());
            }
        }
    }

    private void readSpringConfigAndRemoveUnUsedProperties(File directory) throws SAXException,
            IOException {
        Set<String> processedClasses = getProcessedClassNames();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc = null;
        for (File file : directory.listFiles()) {
            try {
                if (file.isDirectory()) {
                    readSpringConfigAndRemoveUnUsedProperties(file);
                }
                dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(file);
                Document document = $(doc).document();
                $(document).find(BEAN).forEach(bean -> {
                    NodeList childNodes = bean.getChildNodes();
                    if (Objects.nonNull(childNodes)) {
                        for (int i = 0; i < childNodes.getLength(); i++) {
                            Node property = childNodes.item(i);
                            if (property.hasAttributes()) {
                                Node ref = property.getAttributes().getNamedItem("ref");
                                if (Objects.nonNull(ref)) {
                                    String nodeValue = ref.getNodeValue();
                                    if (processedClasses.contains(nodeValue)) {
                                        bean.removeChild(property);
                                    }
                                }
                            }
                        }
                    }
                });
                JavaFileUtil.saveFile(document, file.getName());
            } catch (ParserConfigurationException e) {
                LOGGER.severe(e.getMessage());
            } catch (TransformerFactoryConfigurationError
                    | TransformerException e) {
                LOGGER.severe(e.getMessage());
            }
        }
    }

    private Set<String> getProcessedClassNames() {
        Set<String> processedClasses = ClassVisitorCache.getClassHierarchyVisitor().entrySet().stream()
                .map(entry -> entry.getKey())
                .collect(Collectors.toSet());
        processedClasses.addAll(ClassVisitorCache.getErrorClassNames());
        return processedClasses;
    }

    private Filter unUsedBeanFilter(Set<String> unUsedBeans) {
        return bean -> bean.element().getAttribute(CLASS)
                .contains(ServiceExtractorProperties.ROOT_PACKAGE)
                && unUsedBeans.contains(bean.element().getAttribute(CLASS));
    }

}
