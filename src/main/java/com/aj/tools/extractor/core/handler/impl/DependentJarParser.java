package com.aj.tools.extractor.core.handler.impl;

import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.constant.ServiceExtractorConstants;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.core.handler.spi.Parser;
import com.aj.tools.extractor.util.ServiceExtractorUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static com.aj.tools.extractor.report.ServiceExtractorReport.getUnProcessedJavaFileSet;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class DependentJarParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(DependentJarParser.class.getName());

    private static DependentJarParser instance = new DependentJarParser();

    protected DependentJarParser() {
        // Exists only to defeat instantiation.
    }

    public static DependentJarParser getInstance() {
        if (instance == null) {
            instance = new DependentJarParser();
        }
        return instance;
    }

    @Override
    public void parse() {
        processForJarClasses(getUnProcessedJavaFileSet());
    }


    private void processForJarClasses(Set<JavaFile> unProcessedJavaFileSet) {
        File destLibDir = new File(ServiceExtractorProperties.DEST_LIB_DIR);
        HashSet<String> jarHashSet = new HashSet<>();
        for (JavaFile javaFile : unProcessedJavaFileSet) {
            String className = javaFile.getFullClassName();
            String validClassName = ServiceExtractorUtil
                    .getImportableClassName(className);
            try {
                if (validClassName != null) {
                    Class<?> cl = Class.forName(validClassName);

                    String path = cl.getProtectionDomain().getCodeSource()
                            .getLocation().getPath();
                    String decodedPath = URLDecoder.decode(path,
                            ServiceExtractorConstants.UTF_8);
                    File jarFile = new File(decodedPath);

                    String jarFileName = jarFile.getName();

                    if (!jarHashSet.contains(jarFileName)) {
                        jarHashSet.add(jarFile.getName());
                        FileUtils.copyFileToDirectory(jarFile, destLibDir);
                    }
                }
            } catch (Exception ex) {
                LOGGER.severe(ex.getMessage());
            }

        }
    }

}
