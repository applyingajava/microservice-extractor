package com.aj.tools.extractor.core.handler.impl;

import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.core.handler.spi.Parser;
import com.aj.tools.extractor.util.ServiceExtractorUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.aj.tools.extractor.constant.ServiceExtractorConstants.*;
import static com.aj.tools.extractor.report.ServiceExtractorReport.*;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class StaticImportParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(StaticImportParser.class.getName());

    private static StaticImportParser instance = new StaticImportParser();

    protected StaticImportParser() {
        // Exists only to defeat instantiation.
    }

    public static StaticImportParser getInstance() {
        if (instance == null) {
            instance = new StaticImportParser();
        }
        return instance;
    }

    @Override
    public void parse() {
        try {
            buildImportsClassList();
            setCounter(Parser.process(getCounter()));
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private Predicate<? super String> filterImportsClassList() {
        return className -> (!getProcessedJavaFileSet().contains(className) || !getUnProcessedJavaFileSet()
                .contains(className))
                && className.contains(ServiceExtractorProperties.ROOT_PACKAGE);
    }

    private void buildImportsClassList() {
        getProcessedJavaFileSet().stream().map(this::processImports)
                .flatMap(Collection::stream).filter(filterImportsClassList())
                .collect(Collectors.toSet()).forEach(className -> {
            try {
                ServiceExtractorUtil.buildWorkingJavaFile(className);
            } catch (Exception e) {
                LOGGER.severe(e.getMessage());
            }
        });
    }

    private Set<String> processImports(File jFile) {
        Set<String> classNames = new HashSet<>();

        if (jFile.isFile() && jFile.getName().endsWith(JAVA_EXT)) {
            try (Scanner s = new Scanner(jFile)) {
                String cls;
                while (null != (cls = s.findWithinHorizon(
                        JAVA_IMPORT_PATTERN, 0))) {
                    classNames.add(Stream
                            .of(cls.split(SPACE))
                            .filter(filterImports())
                            .map(this::trimImport).findFirst().get());
                }
            } catch (FileNotFoundException e) {
                LOGGER.severe(e.getMessage());
            }
        }
        return classNames;
    }

    private Predicate<? super String> filterImports() {
        return cName -> !cName.contains(IMPORT) || cName.contains(STATIC);
    }

    private String trimImport(String cName) {
        if (cName.contains(".*")) {
            return cName.substring(0, cName.length() - 3);
        }
        return cName.substring(0, cName.length() - 1);
    }

}
