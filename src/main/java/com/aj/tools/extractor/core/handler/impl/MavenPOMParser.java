package com.aj.tools.extractor.core.handler.impl;

import com.aj.tools.extractor.core.handler.spi.Parser;

import java.util.logging.Logger;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public class MavenPOMParser implements Parser {
    private static final Logger LOGGER = Logger.getLogger(MavenPOMParser.class.getName());

    private static MavenPOMParser instance = new MavenPOMParser();

    protected MavenPOMParser() {
        // Exists only to defeat instantiation.
    }

    public static MavenPOMParser getInstance() {
        if (instance == null) {
            instance = new MavenPOMParser();
        }
        return instance;
    }

    @Override
    public void parse() {
        //to do
    }

}
