package com.aj.tools.extractor.core.handler.spi;

import com.aj.tools.extractor.beans.JavaFile;
import com.aj.tools.extractor.constant.ServiceExtractorProperties;
import com.aj.tools.extractor.util.ServiceExtractorUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.aj.tools.extractor.report.ServiceExtractorReport.*;

/**
 * Created by harsha.vemireddy on 2/19/2017.
 */
public interface Parser {

    void parse();

    public static int process(int counter) throws IOException {
        while (getWorkingJavaFileSet().size() > 0
                && counter < ServiceExtractorProperties.DEPTH_LEVEL) {
            Set<JavaFile> tempJavaFileSet = new HashSet<JavaFile>();
            tempJavaFileSet.addAll(getWorkingJavaFileSet());

            for (JavaFile javaFile : tempJavaFileSet) {
                if (!javaFile.exists()) {
                    // it doesn't exist keep it in the unprocessed cache to
                    // process it later.
                    getUnProcessedJavaFileSet().add(javaFile);
                    getWorkingJavaFileSet().remove(javaFile);
                } else if (getProcessedJavaFileSet().contains(javaFile)) {
                    // its already processed so just remove it
                    getWorkingJavaFileSet().remove(javaFile);
                } else {
                    getProcessedJavaFileSet().add(javaFile);
                    getWorkingJavaFileSet().remove(javaFile);
                    Set<JavaFile> newImportJavaFileSet = ServiceExtractorUtil
                            .getUsedJavaFileList(javaFile.getFullClassName(),
                                    javaFile);

                    newImportJavaFileSet.removeAll(getProcessedJavaFileSet());
                    newImportJavaFileSet.removeAll(getUnProcessedJavaFileSet());

                    if (newImportJavaFileSet.size() != 0) {
                        getWorkingJavaFileSet().addAll(newImportJavaFileSet);
                    }
                }

            }
            counter++;
            // free resources
            tempJavaFileSet.clear();
        }
        return counter;
    }


}
